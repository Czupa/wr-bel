Wrobel::Application.routes.draw do
  resources :sessions, only: [:new, :create, :destroy]
  resources :relationships, only: [:create, :destroy]
  resources :microposts, except: [:edit, :update]
  resources :messages, only: [:new, :create, :destroy]
  resources :favourites, only: [:create, :destroy]
  get 'tag/:name'     => "tags#show",        as: :tag
  get 'logout'        => "sessions#destroy", as: :logout
  get "about"         => "pages#about",      as: :about
  get 'terms'         => "pages#terms",      as: :terms
  get 'cookies_policy'=> "pages#cookies_p",  as: :cookies_policy
  get 'contact'       => "pages#contact",    as: :contact
  get 'mentions'      => "users#mentions",   as: :mentions
  get 'messages'      => "users#threads",    as: :threads
  get 'threads/:id'   => "threads#show",     as: :thread
  get ':name/new_msg' => "messages#new",     as: :new_msg
  get 'new'           => "users#new",        as: :new_user
  get 'search'        => "users#index",      as: :index_user
  root "pages#home"
  resources :users, except: [:new, :index], path: '/' do
    member do
      get :following, :followers, :favourites
    end
  end
  
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end
  
  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end

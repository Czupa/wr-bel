# encoding: utf-8

require 'spec_helper'

describe "Users" do
  describe "signing up" do
    before(:each) do
      visit new_user_path
      fill_in "Nick",  with: "asddasdasdas"
      fill_in "Email", with: "example@example.com"
      fill_in "Hasło", with: "Haslo1"
      fill_in "Potwierdzenie", with: "Haslo1"
      click_link_or_button "Zatwierdź"
      visit logout_path
    end
    it "should create user when valid" do
      lambda do
        visit new_user_path
        fill_in "Nick",          with: "Nick1"
        fill_in "Email",         with: "example@domain.com"
        fill_in "Hasło",         with: "Haslo1"
        fill_in "Potwierdzenie", with: "Haslo1"
        click_button "Zatwierdź"
      end.should change(User, :count).by(1)
    end
    it "should not create user when name taken" do
      lambda do
        visit new_user_path
        fill_in "Nick",          with: "asddasdasdas"
        fill_in "Email",         with: "example@domain.com"
        fill_in "Hasło",         with: "Haslo1"
        fill_in "Potwierdzenie", with: "Haslo1"
        click_button "Zatwierdź"
      end.should_not change(User, :count)
    end
    it "should not create user when passwords doesn't match" do
      lambda do
        visit new_user_path
        fill_in "Nick",          with: "Nick"
        fill_in "Email",         with: "example@domain.com"
        fill_in "Hasło",         with: "Haslo1"
        fill_in "Potwierdzenie", with: "Haslo2"
        click_button "Zatwierdź"
      end.should_not change(User, :count)
    end
    it "should not create user when email taken" do
      lambda do
        visit new_user_path
        fill_in "Nick",          with: "Nick1"
        fill_in "Email",         with: "example@example.com"
        fill_in "Hasło",         with: "Haslo1"
        fill_in "Potwierdzenie", with: "Haslo2"
        click_button "Zatwierdź"
      end.should_not change(User, :count)
    end
  end
end

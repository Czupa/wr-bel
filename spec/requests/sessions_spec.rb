# encoding: utf-8

require 'spec_helper'

describe "Sessions" do
  before(:each) do
    visit new_user_path
    fill_in "Nick",  with: "asddasdasdas"
    fill_in "Email", with: "example@example.com"
    fill_in "Hasło", with: "Haslo1"
    fill_in "Potwierdzenie", with: "Haslo1"
    click_link_or_button "Zatwierdź"
    visit logout_path
  end
  it "should log in when everything valid" do
    fill_in "e-mail", with: "example@example.com"
    fill_in "hasło",  with: "Haslo1"
    click_link_or_button "Zaloguj się"
    page.should have_link "Wyloguj się"
  end
  it "shoul not be able to create user when logged in" do
    fill_in "e-mail", with: "example@example.com"
    fill_in "hasło",  with: "Haslo1"
    click_link_or_button "Zaloguj się"
    visit new_user_path
    page.should have_title("Wróbel | Strona główna")
  end
  
  describe "Friendly forwarding" do
    before(:each) do
      visit new_user_path
      fill_in "Nick",  with: "Użytkownik"
      fill_in "Email", with: "example@domain.com"
      fill_in "Hasło", with: "Haslo1"
      fill_in "Potwierdzenie", with: "Haslo1"
      click_link_or_button "Zatwierdź"
      visit logout_path
    end
    it "should not be able to edit user when not logged in" do
      visit edit_user_path(2)
      fill_in "e-mail", with: "example@example.com"
      fill_in "hasło",  with: "Haslo1"
      click_link_or_button "Zaloguj się"
      page.should have_title("Wróbel | Strona główna")
    end
    it "should be able to edit himself" do
      visit edit_user_path(1)
      fill_in "e-mail", with: "example@example.com"
      fill_in "hasło",  with: "Haslo1"
      click_link_or_button "Zaloguj się"
      page.should have_title("Wróbel | Edytuj profil")
    end
    it "should friendly forward from non-protecting page" do
      visit about_path
      fill_in "e-mail", with: "example@example.com"
      fill_in "hasło",  with: "Haslo1"
      click_link_or_button "Zaloguj się"
      page.should have_title("Wróbel | O")
    end
  end
end

# encoding: utf-8

require 'spec_helper'

describe "StaticPages" do
  it "should have right root page title" do
    visit root_path
    page.should have_title("Wróbel | Strona główna")
  end
  it "should have" do
    visit about_path
    page.should have_title("Wróbel | O")
  end
end

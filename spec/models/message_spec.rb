# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  from_user_id :integer
#  to_user_id   :integer
#  body         :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  thread_id    :integer
#  sender_new   :integer          default(0), not null
#  receiver_new :integer          default(0), not null
#

require 'spec_helper'

describe Message do
  before(:each) do
    @fuser = User.create(name:"Al", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
    @suser = User.create(name:"Ali", email:"exame@example.com",
                          password:"asd123", password_confirmation: "asd123")
    @tuser = User.create(name:"Alik", email:"exame1@example.com",
                          password:"asd123", password_confirmation: "asd123")
    @fuser.follow! @suser
    @suser.follow! @fuser 
    @message = Message.new(from_user: @fuser, to_user: @suser, body:"Raz dwa trzy")
  end
  it "should be valid when everything valid" do
    @message.should be_valid
  end
  it "should respond to from_user" do
    @message.should respond_to(:from_user)
  end
  it "should respond to to_user" do
    @message.should respond_to(:to_user)
  end
  it "should not have body larger than 250" do
    @message.body = 'a' * 251
    @message.should_not be_valid
  end
  it "should respond to threads" do
    Message.should respond_to :threads
  end
  it "should respond to thread_tree" do
    @message.should respond_to :thread_tree
  end
  it "should respond to replies" do
    @message.should respond_to :replies
  end
  it "should not send message when not following" do
    @message.from_user = @tuser
    @message.should_not be_valid
  end

  it "should not create new thread when one exists" do
    @message.save
    message1 = Message.new(from_user: @fuser, to_user: @suser, body:"Raz dwa trzy")
    message1.should_not be_valid
  end
end

# encoding: utf-8
# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  email               :string(255)
#  password_digest     :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  auth_token          :string(255)
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  newmsgcount         :integer          default(0), not null
#  realname            :string(255)
#

require 'spec_helper'

describe User do
  before(:each) do
    @user = User.create(name:"Al", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
  end
  it "should be valid when everything valid" do
    @user.should be_valid
  end
  it "should not have too long name" do
    @user.name = "a" * 31
    @user.should_not be_valid
  end
  it "should match password and password_confirmation" do
    @user.password_confirmation = "asd124"
    @user.should_not be_valid
  end
  it "should have right email" do
    @user.email = 'example.com'
    @user.should_not be_valid
    @user.email = "ak@.com"
    @user.should_not be_valid
    @user.email = '@example.com'
    @user.should_not be_valid
  end
  it "should not have spaces in name" do
    @user.name = "Robert Białas"
    @user.should_not be_valid
  end
  it "should have unique mail" do
    @other_user = User.create(name:"Ali", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
    @other_user.should_not be_valid
    @other_user = User.create(name:"Ali", email:"EXAMPLE@example.com",
                        password:"asd123", password_confirmation: "asd123")
    @other_user.should_not be_valid
  end
  it "should have unique name" do
    @other_user = User.create(name:"Al", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
    @other_user.should_not be_valid
    @other_user = User.create(name:"al", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
    @other_user.should_not be_valid
  end
  it "should not have too short password" do
    @user.password = "a" * 5
    @user.password = "a" * 5
    @user.should_not be_valid
  end
  it "should not have too long password" do
    pass = "a" * 31
    @user.password = pass
    @user.password_confirmation = pass
    @user.should_not be_valid
  end
  it "should respond to microposts" do
    @user.should respond_to :microposts
  end
  it "should respond to feed" do
    @user.should respond_to :feed
  end
  it "should respond to mentions" do
    @user.should respond_to :mentions
  end
  it "should increase mentions_count" do
    lambda do
      @other_user = User.create!(name:"Ali", email:"exame@example.com",
                          password:"asd123", password_confirmation: "asd123")
      @other_user.microposts.create!(content:"@Al")
    end.should change{@user.mentions_microposts.count}.by(1)
  end
  it "should increase mentions_count by one" do
    lambda do
      @other_user = User.create!(name:"Ali", email:"exame@example.com",
                          password:"asd123", password_confirmation: "asd123")
      @other_user.microposts.create!(content:"@Al @Al")
    end.should change{@user.mentions_microposts.count}.by(1)
  end
  it "should respond_to favourites" do
    @user.should respond_to :favourites
  end
  it "should respond to favourite_microposts" do
    @user.should respond_to :favourite_microposts
  end
  it "should not have too long realname" do
    @user.realname = "a" * 41
    @user.should_not be_valid
  end
  it "should not have too short name" do
    @user.realname = "a" * 2
    @user.should_not be_valid
  end

  it "should not have reserved name" do
    @user.name = 'about'
    @user.should_not be_valid
  end

end

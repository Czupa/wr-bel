# encoding: utf-8
# == Schema Information
#
# Table name: microposts
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  content           :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  recrumbed_id      :integer
#  recrumbed_by_id   :integer
#  thread_id         :integer
#  recrumbed_from_id :integer
#

require 'spec_helper'

describe Micropost do
  before(:each) do
    @user = User.create(name:"Imię", email:"example@example.com",
                password:"haslo1", password_confirmation:"haslo1")
    @mp = @user.microposts.create(content:"raz dwa trzy")
    @scuser = User.create(name:"Imię1", email:"example1@example.com",
                password:"haslo1", password_confirmation:"haslo1")
    @thuser = User.create(name:"Imię2", email:"example2@example.com",
                password:"haslo1", password_confirmation:"haslo1")
  end
  it "should respond to user" do
    @mp.should respond_to :user
  end
  it "should not have too large content" do
    @mp.content = "a" * 201
    @mp.should_not be_valid
  end
  it "should not have blank content" do
    @mp.content = " "
    @mp.should_not be_valid
  end
  it "should have user_id" do
    @mp.user_id = nil
    @mp.should_not be_valid
  end
  it "should respond to tags" do
    @mp.should respond_to :tags
  end
  it "should be deleted when user deleted" do
    lambda do
      @user.destroy
    end.should change(Micropost, :count).by(-1)
  end
  it "should create tags" do
    @mp.content = "#lol #nie"
    @mp.save
    @mp.tags.count.should equal(2)
  end
  it "should have related users" do
    @user = User.create(name:"Coś", email:"ex@example.com",
                password:"haslo1", password_confirmation:"haslo1")
    @mp.content = "@Coś"
    @mp.save
    @mp.mentions.count.should eq(1)
  end
  it "should recrumb" do
    @user.follow! @scuser
    @mp1 = @thuser.microposts.create!(content: "Test")
    lambda do
      @scuser.recrumbed_posts.create!(recrumbed_id: 2)
    end.should change(Micropost, :count).by(1)
    @user.feed.size.should equal(1)
    Micropost.count.should equal(3)
  end
  it "should destroy recrumb when micropost destroy" do
    @mp1 = @thuser.microposts.create!(content: "Test")
    lambda do
      @scuser.recrumbed_posts.create!(recrumbed_id: 2)
    end.should change(Micropost, :count).by(1)
    lambda do
      Micropost.find(2).destroy
    end.should change(Micropost, :count).by(-2)
  end
  it "should respond to favourites" do
    @mp.should respond_to :favourites
  end
  it "should respond to favourite_by" do
    @mp.should respond_to :favourite_by
  end
  # it "should not recrumb twice by the same user" do
  #   @a = @mp.recrumbs.create(recrumbed_by_id: 2)
  #   @b = @mp.recrumbs.create(recrumbed_by_id: 2)
  #   @b.should_not be_valid
  # end
end

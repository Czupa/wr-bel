# == Schema Information
#
# Table name: relationships
#
#  id          :integer          not null, primary key
#  follower_id :integer
#  followed_id :integer
#  created_at  :datetime
#  updated_at  :datetime
#

require 'spec_helper'

describe Relationship do
  before(:each) do
    @relationship = Relationship.create(follower_id:2,
                                        followed_id:1)
    @user = User.create(name:"Al", email:"example@example.com",
                        password:"asd123", password_confirmation: "asd123")
  end
  it "should not have blank followed_id" do
    @relationship.followed_id = nil
    @relationship.should_not be_valid
  end
  it "should not have blank follower_id" do
    @relationship.followed_id = nil
    @relationship.should_not be_valid
  end
  it "should not be valid if the same relationship exists" do
    @other_relationship = Relationship.create(follower_id:2,
                                              followed_id:1)
    @other_relationship.should_not be_valid
  end
  it "should be valid when everything valid" do
    @relationship.should be_valid
  end
  it "should respond to follower" do
    @relationship.should respond_to :follower
  end
  it "should respond to followed" do
    @relationship.should respond_to :followed
  end
  it "should be deleted when user deleted" do
    lambda do
      User.first.destroy
    end.should change(Relationship, :count).by(-1)
  end
end

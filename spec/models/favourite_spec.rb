# == Schema Information
#
# Table name: favourites
#
#  id           :integer          not null, primary key
#  micropost_id :integer
#  user_id      :integer
#  created_at   :datetime
#  updated_at   :datetime
#

require 'spec_helper'

describe Favourite do
  before(:each) do
    @fv = Favourite.new(user_id: 1, micropost_id: 1)
  end

  it "should respond to user" do
    @fv.should respond_to :user
  end

  it "should respond to micropost" do
    @fv.should respond_to :micropost
  end

  it "should have user_id" do
    @fv.user_id = nil
    @fv.should_not be_valid
  end

  it "should gave micropost_id" do
    @fv.micropost_id = nil
    @fv.should_not be_valid
  end

  it "should not be valid when same favourite exists" do
    @fv.save
    @fv1 = Favourite.new(user_id: 1, micropost_id: 1)
    @fv1.should_not be_valid
  end
end

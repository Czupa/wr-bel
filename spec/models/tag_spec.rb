# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string(255)
#  created_at :datetime
#  updated_at :datetime
#

require 'spec_helper'

describe Tag do
  before(:each) do
    @t = Tag.new(name:"T")
  end

  it "should not have too long name" do
    @t.name = 'a' * 31
    @t.should_not be_valid
  end

  it "should have name" do
    @t.name = '   '
    @t.should_not be_valid
  end

  it "should be valid when everything valid" do
    @t.should be_valid
  end
  it "should respond to microposts" do
    @t.should respond_to :microposts
  end
  it "should respond to taggings" do
    @t.should respond_to :taggings
  end
end

class CreateMentions < ActiveRecord::Migration
  def change
    create_table :mentions do |t|
      t.belongs_to :user, index: true
      t.belongs_to :micropost, index: true

      t.timestamps
    end
  end
end

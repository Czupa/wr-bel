class AddSeenToMention < ActiveRecord::Migration
  def change
    add_column :mentions, :seen, :boolean, :null => false, :default => false
  end
end

class RemoveMentionsCountFromUser < ActiveRecord::Migration
  def change
    remove_column :users, :mentions_count, :integer
  end
end

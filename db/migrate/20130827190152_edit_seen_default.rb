class EditSeenDefault < ActiveRecord::Migration
  def change
    change_column :mentions, :seen, :boolean, :null => false, :default => false
  end
end

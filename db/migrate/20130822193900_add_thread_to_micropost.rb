class AddThreadToMicropost < ActiveRecord::Migration
  def change
    add_column :microposts, :thread_id, :integer
  end
end

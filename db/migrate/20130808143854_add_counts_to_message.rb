class AddCountsToMessage < ActiveRecord::Migration
  def change
    add_column :messages, :sender_new, :integer, :null => false, :default => 0
    add_column :messages, :receiver_new, :integer, :null => false, :default => 0
  end
end

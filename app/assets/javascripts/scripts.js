var ready = function() {
  $('.mentioned').on('mouseenter', function() {
    $(this).removeClass('mentioned');
  });
  $('.unrecrumb').on('mouseenter', function() {
    // $(this).button("ensable");
    $(this).removeClass('btn-info');
    $(this).addClass('btn-danger');
    // $(this).width('160px');
    $(this).html("<i class='icon-refresh'></i> Przestań przekazywać");
  });
  $('.unrecrumb').on('mouseleave', function() {
    $(this).removeClass('btn-danger');
    $(this).addClass('btn-info');
    $(this).html("<i class='icon-refresh'></i> Przekazałeś ziarno");
    // $(this).width('150px');
    // $(this).button("disable");
  });
  $('.unfavourite').on('mouseenter', function() {
    $(this).removeClass('btn-warning');
    $(this).addClass('btn-danger');
    $(this).html('<i class:"icon-heart"></i> Usuń z ulubionych');
  });
  $('.unfavourite').on('mouseleave', function() {
    $(this).removeClass('btn-danger');
    $(this).addClass('btn-warning');
    $(this).html("<i class:'icon-heart'></i> Dodano do ulubionych");
    //
  });
  $(".reply_button").on('click', function() {
    $(this).next('#reply_form').toggleClass('hiddenmy')
  });
  $('ul.microposts li').on('mouseleave', function() {
    var a = $(this).find('#reply_form');
    if ($(a).html() != undefined && !$(a).hasClass('hiddenmy')) {
      $(a).addClass('hiddenmy');
    }
  });
  $('#newmp').on('keyup', function() {
    var length = $(this).val().length;
    if(length>200)
    {
      $('#newmpbut').attr("disabled", true);
    }
    if(length<=200 && $('#newmpbut').is(":disabled"))
    {
      $('#newmpbut').attr("disabled", false);
    }
    $("#licznik").val(200-length);
  });
};

$(document).ready(ready);
$(document).on('page:load', ready);
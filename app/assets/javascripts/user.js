var follow = function() {
  $('#unfollow').on('mouseenter', function() {
    $(this).removeClass('btn-primary');
    $(this).addClass('btn-danger');
    $(this).val('Przestań obserwować');
  });
  $('#unfollow').on('mouseleave', function() {
    $(this).removeClass('btn-danger');
    $(this).addClass('btn-primary');
    $(this).val('Obserwuję');
  });
};

$(document).ready(follow);
$(document).on('page:load', follow);

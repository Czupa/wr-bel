# encoding: utf-8
class UsersController < ApplicationController
  before_action :not_logged_in, only: [:new, :create]
  before_action :logged_in,     only: [:edit, :update, :delete, :mentions]
  before_action :the_same_user, only: [:edit, :update, :delete]
  #actions above in sessions_helper
  
  def index
    require 'will_paginate/array'
    if params[:search]
      search = User.search{ fulltext params[:search] }.results.to_a
      @users = search.paginate(:page => params[:page])
      if @users.size == 1
        redirect_to user_path @users.first.name
      end
    else
      redirect_back_or root_path
    end
  end

  def show
    @user = User.find(params[:id])
    @microposts = @user.microposts.where('recrumbed_id IS NULL').paginate(:page => params[:page])
  end

  def create
    @user = User.new(user_params)
    if @user.save
      login @user
      redirect_to root_path, notice: "Zarejestrowano i zalogowano"
    else
      render 'new'
    end
  end

  def new
    @user = User.new
  end

  def edit
    @user = User.find(params[:id])
  end

  def update
    @user = User.find(params[:id])
    if @user.authenticate(params[:user][:current_password])
      if params[:user][:password].blank? && params[:user][:password_confirmation].blank?
        params[:user][:password] = params[:user][:current_password]
        params[:user][:password_confirmation] = params[:user][:current_password]
      end
      params[:user].delete :current_password
      if @user.update_attributes(user_params)
        flash[:notice] = "Zaaktalizowano profil"
        redirect_to user_path @user.name
      else
        render 'edit'
      end
    else
      flash.now[:error] = "Nieprawidłowe hasło"
      render 'edit'
    end
  end

  def destroy
    @user = User.find(params[:id])
    if @user.authenticate params[:user][:password]
      @user.destroy
      cookies.delete :auth_token
      flash[:notice] = "Usunięto konto"
      redirect_to root_path
    else
      flash[:error] = "Nieprawidłowe hasło"
      redirect_to edit_user_path @user.name
    end
  end

  def following
    @user = User.find params[:id]
    @title = "#{@user.name} obserwuje"
    @users = @user.followed_users.paginate(page: params[:page])
    render 'show_follow'
  end
  
  def followers
    @user = User.find params[:id]
    @title = "Obserwujący #{@user.name}"
    @users = @user.followers.paginate(page: params[:page])
    render 'show_follow'
  end

  def mentions
    @user = current_user
    @mentions = @user.mentions.paginate(page: params[:page])
  end

  def threads
    @user = current_user
    @threads = @user.threads.sort_by { |a| a.latest_message }.reverse
  end

  def favourites
    @user = User.find params[:id]
    @microposts = @user.favourite_microposts.paginate(page: params[:page])
  end

  private
  
  def user_params
    params.require(:user).permit(:name, :email, :password,
                                 :password_confirmation, 
                                 :avatar, :realname)
  end
end
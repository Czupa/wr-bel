# encoding: utf-8

class SessionsController < ApplicationController
  before_action :logged_in, only: :destroy
  before_action :not_logged_in, only: [:new, :create]
  skip_before_action :last_site
  def new
  end

  def create
    user = User.find_by_email(params[:session][:email])
    if user && user.authenticate(params[:session][:password])
      if params[:session][:remember_me] == "1"
        cookies.permanent[:auth_token] = user.auth_token
      else
        cookies[:auth_token] = user.auth_token
      end
      redirect_back_or root_path
      #delete_remember
      flash[:notice] = "Zalogowano"
    else
      flash.alert = "Nieprawidłowe hasło lub mail"
      redirect_to root_path
    end
  end

  def destroy
    cookies.delete(:auth_token)
    redirect_to root_path, notice:"Wylogowano"
  end
end
class RelationshipsController < ApplicationController
  before_action :logged_in
  skip_before_action :last_site
  def create
    @user = User.find(params[:relationship][:followed_id])
    current_user.follow! @user
    # redirect_to user_name_path @user.name
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

  def destroy
    @user = Relationship.find(params[:id]).followed
    current_user.unfollow! @user
    # redirect_to user_name_path @user.name
    respond_to do |format|
      format.html { redirect_to @user }
      format.js
    end
  end

end

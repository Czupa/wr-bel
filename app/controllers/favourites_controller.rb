class FavouritesController < ApplicationController
  skip_before_action :last_site
  def create
    z = current_user.favourites.create(favourite_params)
    @micropost = z.micropost
    @micropost_id = @micropost.id
    # redirect_back_or(root_path)
    respond_to do |format|
      format.html { redirect_back_or(root_path) }
      format.js
    end
  end

  def destroy
    z = current_user.favourites.find(params[:id])
    @micropost = z.micropost
    @micropost_id = @micropost.id
    z.destroy!
    # redirect_back_or(root_path)
    respond_to do |format|
      format.html { redirect_back_or(root_path) }
      format.js
    end
  end
  private
  def favourite_params
    params.require(:favourite).permit(:micropost_id)
  end
end

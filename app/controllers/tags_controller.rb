class TagsController < ApplicationController
  def show
    @tag = Tag.find_by_name("##{params[:name]}")
    @microposts = @tag.microposts.paginate(:page => params[:page]) unless @tag.nil?
  end
end

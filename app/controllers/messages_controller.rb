# encoding: utf-8

class MessagesController < ApplicationController
  before_action :logged_in
  before_action :must_be_followed,  only:[:new]
  before_action :must_be_followed1, only:[:create]

  def new
    @user = User.find_by_name params[:name]
    if @user.thread_exists? current_user
      redirect_to thread_path(@user.thread_with current_user)
    end
  end

  def create
    @msg = current_user.messages_sent.new(message_params)
    if @msg.save
      redirect_to thread_path(@msg.thread_id || @msg.id)
    else
      redirect_back_or(root_url)
      flash[:error] = "Treść nieprawidłowa"
    end
  end

  private
  
  def must_be_followed
    unless User.find_by_name(params[:name]).following? current_user
      redirect_to root_url
    end
  end

  def must_be_followed1
    unless User.find(params[:message][:to_user_id]).following? current_user
      redirect_back_or root_url
      flash[:error] = "Musisz być obserwowany przez #{User.find(params[:message][:to_user_id]).name} wysłać wiadomość"
    end
  end

  def message_params
    params.require(:message).permit(:body, :to_user_id, 
                                    :thread_id)
  end
end

# encoding: utf-8

class MicropostsController < ApplicationController
  def create
    @user = current_user
    @mp = @user.microposts.new(micropost_params)
    if @mp.save && !@mp.recrumbed_id
      if @mp.thread_id
        flash[:notice] = "Odpowiedziano"
      else
        flash[:notice] = "Stworzono ziarno"
      end
    elsif !@mp.recrumbed_id
      flash[:error] = "Nieprawidłowa zawartość"
    end
    respond_to do |format|
    if(@mp.recrumbed_id)
      format.html { redirect_to :back }
      format.js
    else
      format.html{ redirect_to :back }
    end
  end
  end

  def destroy
    @mp = Micropost.find(params[:id])
    if @mp.recrumbed_by_id
      if current_user.id == @mp.recrumbed_by_id
        @mp.destroy!
      end
    else 
      if current_user == @mp.user
        @mp.destroy!
        flash[:notice] = "Usunięto"
      end
    end
    respond_to do |format|
      if @mp.recrumbed_id
        format.html { redirect_to :back }
        format.js
      else
        format.html{ redirect_to :back }
      end
    end
  end

  def show
    @mp = Micropost.find(params[:id])
  end

  private
  def micropost_params
    params.require(:micropost).permit(:content, :recrumbed_id, :thread_id, :recrumbed_from_id)
  end
end

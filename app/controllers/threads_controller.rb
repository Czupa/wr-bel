class ThreadsController < ApplicationController
  #Thread is part of message model
  #member of thread is sessions helper action
  before_action :member_of_thread
  def show
    @user = current_user
    @thread = Message.find(params[:id])
    @messages = @thread.thread_tree
    if current_user == @thread.to_user
      @partner = @thread.from_user
      if @user.newmsgcount > 0
        @user.decrease_newmsgcount!(@thread.receiver_new)
        @thread.reset_receiver_new
      end
    else
      @partner = @thread.to_user
      if @user.newmsgcount > 0
        @user.decrease_newmsgcount!(@thread.sender_new)
        @thread.reset_sender_new
      end
    end
  end
end
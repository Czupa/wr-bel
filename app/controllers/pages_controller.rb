class PagesController < ApplicationController
  def home
    @user = current_user || User.new
    @microposts = current_user.feed.paginate(:page => params[:page]) if current_user
  end

  def about
  end
  
  def terms
  end

  def cookies_p
  end

  def contact
  end
end

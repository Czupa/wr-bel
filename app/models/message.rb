# encoding: utf-8
# == Schema Information
#
# Table name: messages
#
#  id           :integer          not null, primary key
#  from_user_id :integer
#  to_user_id   :integer
#  body         :string(255)
#  created_at   :datetime
#  updated_at   :datetime
#  thread_id    :integer
#  sender_new   :integer          default(0), not null
#  receiver_new :integer          default(0), not null
#

class Message < ActiveRecord::Base
  before_create { create_thread }
  before_create { increase_newmsg_count }
  before_create { increase_new_count }

  belongs_to :from_user, class_name: 'User'
  belongs_to :to_user,   class_name: 'User'
  belongs_to :thread,    class_name: 'Message'

  has_many :replies,     class_name: 'Message',
                         foreign_key: 'thread_id',
                         dependent: :destroy

  validates :from_user_id, presence: true
  validates :to_user_id,   presence: true
  validates :body, length: { maximum: 250 },
                   presence: true
  validate :receiver_must_follow
  validate :one_thread


  def create_thread
    if thread.nil?
      self.thread_id = nil
    else
      self.thread_id = thread.id
    end
  end

  def increase_newmsg_count
    user = User.find(to_user_id)
    user.increase_newmsgcount!
  end

  def increase_new_count
    user = User.find(to_user_id)
    if thread_id == nil
      self.receiver_new = 1
    elsif thread.to_user == user
      thread.increment!(:receiver_new)
    else
      thread.increment!(:sender_new)
    end
  end

  def in_reply_to
    Message.where('id = ?', thread_id)
  end

  def self.threads
    where('thread_id IS NULL')
  end

  def self.user_threads(user)
    threads.where('from_user_id = :id OR to_user_id = :id',
                  id: user.id)
  end

  def thread_tree
    Message.where('id = :id OR thread_id = :id', id: id)
  end

  def latest_message
    Message.where('id = :id OR thread_id = :id', id: id).first
  end

  def reset_sender_new
    update_attribute(:sender_new, 0)
  end

  def reset_receiver_new
    update_attribute(:receiver_new, 0)
  end

  private

  def self.default_scope
    order 'created_at DESC'
  end

  def receiver_must_follow
    if to_user_id.nil?
      user = nil
    else
      user = User.find(to_user_id)
    end
    if user.nil?
      errors[:to_user_id] << 'Nie ma takiego użytkownika'
    elsif !(user.following? User.find(from_user_id))
      errors[:to_user_id] << "#{user.name} nie obserwuje Cię"
    end
  end

  def one_thread
    if thread_id.nil? && Message.threads.where('(to_user_id = :id AND from_user_id = :sid) OR (to_user_id = :sid AND from_user_id = :id)', id: from_user_id, sid: to_user_id).any?
      errors[:base] << 'Już istnieje wątek rozmowy między wami'
    end
  end
end

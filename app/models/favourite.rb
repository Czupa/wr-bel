# == Schema Information
#
# Table name: favourites
#
#  id           :integer          not null, primary key
#  micropost_id :integer
#  user_id      :integer
#  created_at   :datetime
#  updated_at   :datetime
#

class Favourite < ActiveRecord::Base
  belongs_to :micropost
  belongs_to :user

  validates :micropost_id, presence: true
  validates :user_id, presence: true
  validates_uniqueness_of :user_id, scope: :micropost_id
end

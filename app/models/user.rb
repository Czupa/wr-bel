# encoding: utf-8

# == Schema Information
#
# Table name: users
#
#  id                  :integer          not null, primary key
#  name                :string(255)
#  email               :string(255)
#  password_digest     :string(255)
#  created_at          :datetime
#  updated_at          :datetime
#  auth_token          :string(255)
#  avatar_file_name    :string(255)
#  avatar_content_type :string(255)
#  avatar_file_size    :integer
#  avatar_updated_at   :datetime
#  newmsgcount         :integer          default(0), not null
#  realname            :string(255)
#

class User < ActiveRecord::Base
  before_create { generate_token(:auth_token) }
  has_secure_password
  has_attached_file :avatar, styles: { medium: '300x300>', thumb: '100x100>', small: '50x50>' }, default_url: '/user50.png'
  has_many :microposts, dependent: :destroy
  has_many :messages_received, class_name: 'Message',
                               foreign_key: 'to_user_id'
  has_many :messages_sent, class_name: 'Message',
                           foreign_key: 'from_user_id'
  has_many :relationships,
           foreign_key: 'follower_id',
           dependent: :destroy
  has_many :followed_users,
           through: :relationships, source: :followed
  has_many :reverse_relationships,
           foreign_key: 'followed_id',
           class_name: 'Relationship',
           dependent: :destroy
  has_many :followers,
           through: :reverse_relationships, source: :follower
  has_many :mentions, dependent: :destroy
  has_many :mentions_microposts, through: :mentions, source: :micropost
  has_many :recrumbed_posts,  class_name: 'Micropost',
                              foreign_key: 'recrumbed_by_id',
                              dependent: :destroy
  has_many :favourites, dependent: :destroy
  has_many :favourite_microposts, through: :favourites,
                                  source: :micropost

  email_regex       = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
  no_spaces_regex   = /\A[\S]+\z/

  validates_format_of :email, with: email_regex
  validates_format_of :name,  with: no_spaces_regex
  validates :email, presence: true
  validates :name, length: { maximum: 30 },
                   presence: true
  validates :password_confirmation, presence: true
  validates :password, presence: true
  validates_length_of :password, within: 6..30
  validates_length_of :realname, within: 3..40, allow_blank: true
  validates_uniqueness_of :name, case_sensitive: false
  validates_uniqueness_of :email, case_sensitive: false
  validate :reserved_name

  searchable do
    text :name
    text :realname
  end

  def feed
    Micropost.from_users_followed_by(self)
  end

  def following?(other_user)
    !!relationships.find_by(followed_id: other_user.id)
  end

  def follow!(other_user)
    relationships.create!(followed_id: other_user.id)
  end

  def unfollow!(other_user)
    relationships.find_by(followed_id: other_user.id).destroy
  end

  def mentions_count
    mentions.where('seen = ?', false).count
  end

  def reset_mentions_count
    mentions.where('seen = ?', false).update_all(seen: true)
  end

  def self.find_by_similar_name(name)
    where('name LIKE ?', name).first
  end

  def threads
    Message.user_threads(self)
  end

  def thread_exists?(user)
    Message.threads.where('(to_user_id = :cid AND from_user_id = :id) OR (to_user_id = :id AND from_user_id = :cid)', id: user.id, cid: id).any?
  end

  def thread_with(user)
    Message.threads.where('(to_user_id = :cid AND from_user_id = :id) OR (to_user_id = :id AND from_user_id = :cid)', id: user.id, cid: id).first
  end

  def increase_newmsgcount!
    increment!(:newmsgcount)
  end

  def decrease_newmsgcount!(l)
    decrement!(:newmsgcount, l)
  end

  def favourite?(mp)
    favourites.find_by(micropost_id: mp.id)
  end

  def favourite(mp)
    favourites.find_by(micropost_id: mp.id)
  end

  def generate_token(name)
    self[name] = SecureRandom.urlsafe_base64
    if User.exists?(name => self[name])
      generate_token(name)
    end
  end

  def self.find(input)
    input.to_i == 0 ? find_by_similar_name(input) : super
  end

  private
  require 'set'
  RESERVED_NAMES = Set.new(%w{
    about account add admin api
    app apps archive archives auth
    blog
    cookies_policy config connect contact create
    delete direct_messages downloads
    edit email
    faq favorites favourites feed feeds follow followers following
    help home
    invitations invite
    jobs
    login log-in log_in logout log-out log_out logs
    map maps mentions messages
    new_msg
    oauth oauth_clients openid
    privacy
    register remove replies rss
    save search sessions settings
    search signup sign-up sign_up signin sign-in sign_in signout sign-out sign_out
    sitemap ssl subscribe
    tag terms test threads trends
    unfollow unsubscribe url user
    widget widgets
    xfn xmpp
  })

  def reserved_name
    if RESERVED_NAMES.include?(name)
      errors[:name] << 'Imię zarezerwowane'
    end
  end
end

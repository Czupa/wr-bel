# encoding: utf-8

# == Schema Information
#
# Table name: microposts
#
#  id                :integer          not null, primary key
#  user_id           :integer
#  content           :string(255)
#  created_at        :datetime
#  updated_at        :datetime
#  recrumbed_id      :integer
#  recrumbed_by_id   :integer
#  thread_id         :integer
#  recrumbed_from_id :integer
#

require 'set'
class Micropost < ActiveRecord::Base
  before_create { tags }
  before_create { mentions }
  before_validation { from_recrumbed }

  belongs_to :user
  belongs_to :recrumbed, class_name: 'Micropost'
  belongs_to :recrumbed_by, class_name: 'User'
  belongs_to :thread, class_name: 'Micropost'
  belongs_to :recrumbed_from, class_name: 'Micropost'

  has_many :taggings, dependent: :destroy
  has_many :mentions, dependent: :destroy
  has_many :tags, through: :taggings
  has_many :related_users, through: :mentions,
                           source:  :user
  has_many :recrumbs, class_name:  'Micropost',
                      foreign_key: 'recrumbed_id',
                      dependent:   :destroy
  has_many :favourites, dependent: :destroy
  has_many :favourite_by, through: :favourites,
                          source:  :user
  has_many :replies, class_name:  'Micropost',
                     foreign_key: 'thread_id',
                     dependent:   :destroy
  has_many :recrumbs_from, class_name:  'Micropost',
                           foreign_key: 'recrumbed_from_id',
                           dependent:   :destroy

  validates :user_id, presence: true
  validates :content, presence: true,
                      length: { maximum: 200 }
  validate :one_recrumb

  def self.from_users_followed_by(user)
    followed_user_ids = "SELECT followed_id FROM relationships
                         WHERE follower_id = :user_id"
    where("(((user_id IN (#{followed_user_ids}) AND thread_id IS NULL) OR user_id = :user_id) AND recrumbed_id IS NULL) OR (recrumbed_by_id IN (#{followed_user_ids}) AND user_id NOT IN ((#{followed_user_ids})) AND user_id != :user_id)", user_id: user.id)
    # where("user_id IN (#{followed_user_ids}) OR user_id = :user_id", user_id: user.id)
  end

  def tags
    tag_set = Set.new
    content.scan(/#\S+/).map do |n|
      tag_set << Tag.where(name: n.strip).first_or_create!
    end
    self.tags = tag_set.to_a
  end

  def mentions
    if recrumbed_id == nil
      mentioned_users = Set.new
      content.scan(/@\S+/).each do |n|
        name = n[1..-1]
        @user = User.find_by_similar_name(name)
        if @user && @user.name != user.name
          mentioned_users.add @user
        else
          next
        end
      end
      self.related_users = mentioned_users.to_a
    end
  end

  def from_recrumbed
    if recrumbed_id != nil
      mp = Micropost.find(recrumbed_id)
      self.recrumbed_by_id = user_id
      self.content = mp.content
      self.user_id = mp.user.id
    end
  end

  def recrumbed?(user)
    recrumbs.where('recrumbed_by_id = ?', user.id)
  end

  private
  def self.default_scope
    order 'created_at DESC'
  end

  def one_recrumb
    if recrumbed_id && recrumbed.recrumbs.where('recrumbed_by_id = ?', user.id).any?
      errors.add(:recrumbed_id, 'Już przekazałeś ten okruch')
    end
  end
end

# == Schema Information
#
# Table name: mentions
#
#  id           :integer          not null, primary key
#  user_id      :integer
#  micropost_id :integer
#  created_at   :datetime
#  updated_at   :datetime
#  seen         :boolean          default(FALSE), not null
#

class Mention < ActiveRecord::Base
  belongs_to :user
  belongs_to :micropost

  private

  def self.default_scope
    order 'created_at DESC'
  end
end

# encoding: utf-8

module SessionsHelper
  private
  def current_user
    @current_user ||= User.find_by_auth_token!(cookies[:auth_token]) if cookies[:auth_token]
  end

  def current_name
    current_user.name
  end

  def login(user)
    cookies[:auth_token] = user.auth_token
  end

  def redirect_back_or(url)
    redirect_to(session[:return_to] || session[:last_site] || url)
    delete_return_to
  end

  def store_location
    session[:return_to] = request.original_url
  end
  
  def delete_return_to
    session.delete :return_to
  end

  def last_site
    session[:last_site] = request.original_url if request.get?
  end

  def logged_in
    unless current_user
      store_location
      redirect_to root_path, notice: "Musisz się zalogować; po zalogowaniu zostaniesz przekierowany na pożądany adres"
    end
  end

  def not_logged_in
    redirect_to root_path if current_user
  end

  def the_same_user
    @user = User.find(params[:id])
    redirect_to root_path unless current_user == @user
  end

  def member_of_thread
    @thread = Message.find(params[:id])
    unless current_user == @thread.to_user || current_user == @thread.from_user
      redirect_to root_path
    end
  end
end
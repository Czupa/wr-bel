module ApplicationHelper
  require 'uri'
  require 'set'
  def mcontent(mp)
    tags = Set.new mp.content.scan(/#\S+/)
    names = Set.new mp.content.scan(/@\S+/)
    links = Set.new URI.extract(mp.content, ['http', 'https'])
    html = CGI::escapeHTML(mp.content)
    tags.each do |t|
      html.gsub!("#{t}",
      (link_to(t, tag_path(t[1..-1]))))
    end
    names.each do |n|
      name = n[1..-1]
      user = User.find_by_similar_name(name)
      html.gsub!("#{n}",
      (link_to(n, user_path(user.name)))) if user
    end
    links.each do |n|
      link = URI(n).host
        html.gsub!("#{n}",
                   link_to(link, n))
      end
    html.html_safe
  end
end